##
# gateware_builder.nix
#
# Produce an attrset of gateware from a folder of system description json files
#
# This module outputs a function designed to be called by callPackage (the usual Nix convention).
# You must also provide `descriptor_src` = a folder of .json system descriptors.
#
# Outputs:
#  dict of name : gateware, one per JSON file, where
#     name = e.g. artiq-board-kasli-cammaster
#     gateware = a folder containing the build gateware for that system

{ descriptor_src, lib, stdenv, artiq_flake }:

let
  makeArtiqBoardPackage = artiq_flake.makeArtiqBoardPackage;

  json_files = builtins.attrNames (
    lib.filterAttrs
      (name: type:
        type != "directory" &&
        builtins.match ".+\\.json" name != null
      )
      (builtins.readDir descriptor_src)
  );

  json_descriptors = map
    (
      json_file:
      builtins.fromJSON (
        builtins.readFile (descriptor_src + "/${json_file}")
      )
    )
    json_files;

  systemsList = (builtins.map
    (json_file:
      let
        file_path = (descriptor_src + "/${json_file}");
        parsed_json_file = builtins.fromJSON (
          builtins.readFile file_path
        );
        variant = parsed_json_file.variant;

        output = makeArtiqBoardPackage {
          target = "kasli";
          inherit variant;
          buildCommand = "python -m artiq.gateware.targets.kasli ${file_path} && ls -la";
        };

      in
      {
        name = variant;
        value = output;
      }
    )
    json_files);

  joinCmd = builtins.toString (builtins.map
    ({ name, value }: ''
      mkdir ${name}
      cp -r ${value}/. ${name}
    '')
    systemsList);

in
{
  all = stdenv.mkDerivation {
    name = "allsystems";
    phases = [ "buildPhase" ];
    buildPhase = ''
      mkdir $out
      cd $out
    '' + joinCmd;
  };
} // builtins.listToAttrs systemsList
