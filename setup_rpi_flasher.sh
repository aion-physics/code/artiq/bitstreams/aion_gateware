#!/bin/bash
# This script will set up a bare-bones RPI running rasbian with all the
# requirements for flashing ARTIQ boards remotely. Once this has been done, you
# can pass "--host username@ipaddress" of this RPI to the artiq_flash script on
# another computer to remotely flash a crate.

ARTIQ_FORK="git+https://gitlab.com/aion-physics/code/artiq/forks/artiq_fork.git"
BOOTSTRAP_SCRIPT="https://gitlab.com/aion-physics/code/artiq/artiq_training/-/raw/713a1a52a5e0aebcc0fc3a5e03196ac5191d3cf2/bootstrap.sh"

echo Setting up a new RPI as a flasher board

read -p "Enter newly-flashed RPI's hostname:" hostname
read -p "Enter newly-flashed RPI's username:" username

ssh_target=${username}@${hostname}

echo Installing SSH certificates. Please enter the RPI\'s SSH password:
ssh-copy-id ${ssh_target}

echo Setting up nix etc
ssh ${ssh_target} wget ${BOOTSTRAP_SCRIPT} -O bootstrap.sh
ssh ${ssh_target} chmod +x bootstrap.sh
ssh ${ssh_target} ./bootstrap.sh

echo Installing OpenOCD from AION ARTIQ fork
# This needs a login shell to get the nix environmental variables
ssh ${ssh_target} "bash -l -c 'nix profile install --accept-flake-config --refresh ${ARTIQ_FORK}#openocd-bscanspi'"
