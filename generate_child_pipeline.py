import json
from glob import glob
from pathlib import Path

SYSTEMS_DIR = Path(__file__, "../systems").resolve()


systems_json_files = glob(str(SYSTEMS_DIR / "*.json"))

pipeline_YAML = ""

for file in systems_json_files:
    desc = json.load(open(file))
    name = desc["variant"]

    pipeline_YAML += f"""
{name}:
  extends: .nixbuilder
  variables:
    NIX_BUILD_TARGET: {name}
  rules:
    - when: always
      changes:
      - default.nix
      - flake.nix
      - flake.lock
      - systems/{Path(file).name}
      - child-pipeline-template.yml
    - when: manual
      allow_failure: true
"""

print(pipeline_YAML)
