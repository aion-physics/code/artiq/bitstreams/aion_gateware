##
# gateware_builder.nix
#
# Produce an attrset of gateware from a folder of system description json files
#
# This module outputs a function designed to be called by callPackage (the usual Nix convention).
# You must also provide `descriptor_src` = a folder of .json system descriptors.
#
# Outputs:
#  dict of name : gateware, one per JSON file, where
#     name = e.g. artiq-board-kasli-cammaster
#     gateware = a folder containing the build gateware for that system

{ descriptor_src, stdenv, artiq_flake }:
let
  reqs = artiq_flake.devShells.x86_64-linux.boards.buildInputs;

  mk-device-db = { master, satellite }:
    let
      variant = (builtins.fromJSON (
        builtins.readFile master
      )).variant;

    in
    stdenv.mkDerivation {
      name = "device-db-${variant}";
      phases = [ "buildPhase" ];
      buildInputs = [ reqs ];
      buildPhase = ''
        artiq_ddb_template -s 1 ${satellite} ${master} > $out
      '';
    };
in
{
  # Do the ICL ones separately since they have more than one satellite
  device-db-icl = stdenv.mkDerivation {
    name = "device-db-iclmaster";
    phases = [ "buildPhase" ];
    buildInputs = [ reqs ];
    buildPhase = ''
      artiq_ddb_template \
        -s 1 "${descriptor_src}/icl_satellite_blue.json" \
        -s 2 "${descriptor_src}/icl_satellite_red.json" \
        -s 3 "${descriptor_src}/icl_satellite_plantroom.json" \
      "${descriptor_src}/icl_master.json" > $out
    '';
  };

  #ICL's USOC crates
  device-db-usoc = stdenv.mkDerivation {
    name = "device-db-usocmain";
    phases = [ "buildPhase" ];
    buildInputs = [ reqs ];
    buildPhase = ''
      artiq_ddb_template \
        -s 1 "${descriptor_src}/icl_usoc_suservo_1.json" \
        -s 2 "${descriptor_src}/icl_usoc_suservo_2.json" \
        -s 3 "${descriptor_src}/icl_usoc_plantroom.json" \
      "${descriptor_src}/icl_usoc_main.json" > $out
    '';
  };

  device-db-icl-training = stdenv.mkDerivation {
    name = "device-db-icltraining";
    phases = [ "buildPhase" ];
    buildInputs = [ reqs ];
    buildPhase = ''
      artiq_ddb_template \
      "${descriptor_src}/icl_standalone_training.json" > $out
    '';
  };

  device-db-bham = mk-device-db {
    master = "${descriptor_src}/ubirmingham3master.json";
    satellite = "${descriptor_src}/ubirmingham3satellite.json";
  };

  device-db-cam = mk-device-db {
    master = "${descriptor_src}/cam_master.json";
    satellite = "${descriptor_src}/cam_satellite1.json";
  };

  device-db-ox = mk-device-db {
    master = "${descriptor_src}/ox_master.json";
    satellite = "${descriptor_src}/ox_satellite1.json";
  };

  device-db-ral = mk-device-db {
    master = "${descriptor_src}/ral_master.json";
    satellite = "${descriptor_src}/ral_satellite1.json";
  };
}
