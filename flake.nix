{
  description = "A build of all the AION gateware";

  inputs.artiq.url = "git+https://gitlab.com/aion-physics/code/artiq/forks/artiq_fork.git?ref=make-event-spreading-optional";
  inputs.nixpkgs.follows = "artiq/nixpkgs";

  outputs = { self, nixpkgs, artiq }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      packages = artiq.packages.x86_64-linux // { artiq_flake = artiq; };
      callPackage = pkgs.lib.callPackageWith (pkgs // pkgs.python3Packages // packages);
    in
    {
      images = callPackage ./gateware_builder.nix { descriptor_src = ./systems; };
      device_dbs = callPackage ./build_device_dbs.nix { descriptor_src = ./systems; };
      devShell.x86_64-linux = artiq.devShells.x86_64-linux.boards;

      apps.x86_64-linux.flash =
        let
          versionHash = if (self ? rev) then self.rev else "dirty";
          artiqVersionHash = if (artiq ? rev) then artiq.rev else "dirty";
          script = pkgs.writeShellScriptBin "run" ''
            export PATH=${pkgs.lib.makeBinPath self.devShell.x86_64-linux.buildInputs}:$PATH

            printf \\n
            echo "*****************************************************"
            echo Flashing connected ARTIQ board
            echo Variant: "$1"
            echo aion_gateware version: "${versionHash}"
            echo artiq version: "${artiqVersionHash}"
            echo "*****************************************************"
            printf \\n

            set -e
            set -x

            built_image=$(nix build --print-out-paths --no-link ${self}#images.$1)

            artiq_flash -d "$built_image" "''${@:2}"
          '';
        in
        { type = "app"; program = "${script}/bin/run"; };
      apps.x86_64-linux.default = self.apps.x86_64-linux.flash;
    };

  nixConfig = {
    extra-trusted-public-keys = "aion-physics.cachix.org-1:6nSnNuBFRf4kl9EPG6hAMQHBjcrrKEfHG2BpHBE2DVs=";
    extra-substituters = "https://aion-physics.cachix.org";
    extra-sandbox-paths = "/opt";
  };
}
