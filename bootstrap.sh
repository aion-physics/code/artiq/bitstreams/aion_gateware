## bootstrap.sh
#
# Run this file to quickly set up the environment that a docker builder will
# use. This should only be in inside new docker containers, otherwise it'll mess
# with your system nix setup. Be warned!
#
# Use this command (after authenticating with the gitlab docker registry) to set
# up a temporary docker build environment with a persistant nix cache:
#
# docker run --privileged -v nix:/nix --rm -it registry.gitlab.com/aion-physics/code/vivado-docker:2021.1

# Set up nix env vars:
source /home/nixuser/.nix-profile/etc/profile.d/nix.sh

# Trust the M-labs binary cache
mkdir -p ~/.local/share/nix
echo '{"extra-sandbox-paths":{"/opt":true},"extra-substituters":{"https://nixbld.m-labs.hk":true},"extra-trusted-public-keys":{"nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc=":true}}' > ~/.local/share/nix/trusted-settings.json

# Enable flakes
mkdir -p ~/.config/nix
echo experimental-features = nix-command flakes >> ~/.config/nix/nix.conf

# Build
echo "Preparing environment..."
nix develop -c echo Environment build completed

# Alter this to test another target:
export NIX_BUILD_TARGET=artiq-board-kasli-iclmaster

echo "Building..."
nix-build --no-sandbox -A ${NIX_BUILD_TARGET}
echo "Build complete!"
ls -la result
